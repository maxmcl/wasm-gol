const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require('path');


module.exports = {
  entry: [
    "./src/bootstrap.ts"
  ],
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
  },
  target: "web",
  mode: "development",
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {from: "public/index.html", to: "index.html"},
      ]
    }
    )
  ],
  resolve: {
    extensions: [".js", ".ts", '.wasm']
  },
  experiments: {asyncWebAssembly: true},
  module: {
    rules: [{
      include: [
        path.resolve(__dirname, "js")
      ],
    },
    {
      test: /\.js$/,
      exclude: ["/node_modules/"],
      loader: "source-map-loader"
    },
    {
      test: /\.ts?$/,
      use: 'ts-loader',
      exclude: /node_modules/
    }
    ]
  }
};
