import {memory} from "wasm-gol/wasm_gol_bg.wasm";
import {Universe, Cell} from "wasm-gol";

const CELL_SIZE = 5; // px
const GRID_COLOR = "#CCCCCC";
const DEAD_COLOR = "#FFFFFF";
const ALIVE_COLOR = "#000000";

const universe = Universe.new(200, 100);
universe.set_alive(4, 4);
universe.set_alive(4, 6);
universe.set_alive(6, 5);
universe.set_alive(5, 7);
universe.set_alive(7, 7);
universe.set_alive(5, 5);
universe.set_alive(7, 5);
universe.set_alive(7, 8);
universe.set_alive(9, 8);
universe.set_alive(5, 4 + 4);
universe.set_alive(6, 2 + 4);

const canvas = document.getElementById("game-of-life-canvas") as HTMLCanvasElement;
canvas.width = (CELL_SIZE + 1) * universe.width() + 1;
canvas.height = (CELL_SIZE + 1) * universe.height() + 1;

const ctx = canvas.getContext("2d") as CanvasRenderingContext2D;

const drawGrid = () => {
  ctx.beginPath();
  ctx.strokeStyle = GRID_COLOR;

  for (let i = 0; i <= universe.width(); i++) {
    ctx.moveTo(i * (CELL_SIZE + 1) + 1, 0);
    ctx.lineTo(i * (CELL_SIZE + 1) + 1, (CELL_SIZE + 1) * universe.height() + 1);
  }

  for (let j = 0; j <= universe.height(); j++) {
    ctx.moveTo(0, j * (CELL_SIZE + 1) + 1);
    ctx.lineTo((CELL_SIZE + 1) * universe.width() + 1, j * (CELL_SIZE + 1) + 1);
  }

  ctx.stroke();
}

const getIndex = (row: number, column: number): number => {
  return row * universe.width() + column;
};

const drawCells = () => {
  const cellsPtr = universe.cells();
  const cells = new Uint8Array(memory.buffer, cellsPtr, universe.width() * universe.height());

  ctx.beginPath();

  for (let row = 0; row < universe.height(); row++) {
    for (let col = 0; col < universe.width(); col++) {
      const idx = getIndex(row, col);

      ctx.fillStyle = cells[idx] === Cell.Dead
        ? DEAD_COLOR
        : ALIVE_COLOR;

      ctx.fillRect(
        col * (CELL_SIZE + 1) + 1,
        row * (CELL_SIZE + 1) + 1,
        CELL_SIZE,
        CELL_SIZE
      );
    }
  }

  ctx.stroke();
};

const renderLoop = () => {
  universe.tick();
  drawGrid();
  drawCells();
  requestAnimationFrame(renderLoop);
};

drawGrid();
drawCells();
requestAnimationFrame(renderLoop);
