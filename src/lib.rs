mod utils;
use std::fmt;

use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Cell {
    Dead = 0,
    Alive = 1,
}

#[wasm_bindgen]
pub struct Universe {
    width: u32,
    height: u32,
    cells: Vec<Cell>,
}

impl fmt::Display for Universe {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for line in self.cells.as_slice().chunks(self.width as usize) {
            for &cell in line {
                write!(
                    f,
                    "{}",
                    match cell {
                        Cell::Dead => '◻',
                        Cell::Alive => '◼',
                    }
                )?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Universe {
    fn get_index(&self, row: u32, column: u32) -> usize {
        (self.width * row + column) as usize
    }

    fn live_neighbor_count(&self, row: u32, column: u32) -> u8 {
        // Check cells in neighborhood, offsets [-2, -1, 0, 1, 2] except [0, 0]
        let mut count = 0u8;
        for row_offset in [self.height - 1, 0, 1] {
            for col_offset in [self.width - 1, 0, 1] {
                if row_offset == 0 && col_offset == 0 {
                    continue;
                }
                if let Some(cell) = self.cells.get(self.get_index(
                    (row + row_offset) % self.height,
                    (column + col_offset) % self.width,
                )) {
                    count += (*cell) as u8;
                }
            }
        }
        count
    }
}

#[wasm_bindgen]
impl Universe {
    pub fn new(width: u32, height: u32) -> Self {
        Self {
            width,
            height,
            cells: vec![Cell::Dead; (width * height) as usize],
        }
    }

    pub fn set_alive(&mut self, row: u32, column: u32) {
        let index = self.get_index(row, column);
        if let Some(cell) = self.cells.get_mut(index) {
            *cell = Cell::Alive;
        }
    }

    pub fn tick(&mut self) {
        for row in 0..self.height {
            for col in 0..self.width {
                let index = self.get_index(row, col);
                let count = self.live_neighbor_count(row, col);
                if let Some(cell) = self.cells.get_mut(index) {
                    *cell = match (&cell, count) {
                        (Cell::Alive, x) if x < 2 => Cell::Dead,
                        (Cell::Alive, 2) | (Cell::Alive, 3) => Cell::Alive,
                        (Cell::Alive, x) if x > 3 => Cell::Dead,
                        (Cell::Dead, 3) => Cell::Alive,
                        (otherwise, _) => **otherwise,
                    }
                }
            }
        }
    }
    pub fn render(&self) -> String {
        format!("{}", self)
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn cells(&self) -> *const Cell {
        self.cells.as_ptr()
    }
}
